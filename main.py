import sys
import json


def check_args(arguments):
    """!
        Checks if file name was passed as an argument
        @param arguments Takes in passed arguments to script
    """

    if len(arguments) == 2:
        return arguments[1]
    else:
        print("Usage: python3 main.py filename")
        sys.exit(2)


def gather_data(filename, dict_of_events, dict_of_times):
    """!
        Takes data from log file and splits into times and events dictionary
        @param filename filename that was passed to a script as an argument
        @param dict_of_events dictionary that will be filled with events
        @param dict_of_times dictionary that will be filled with times
    """
    file = open(filename, "r")
    for element in file:
        split_list = element.split()
        dict_of_events.setdefault(split_list[1], []).append(split_list[2])
        dict_of_times.setdefault(split_list[1], []).append(split_list[0])
    file.close()


def clean_data(dict_of_events, dict_of_times):
    """!
        Takes data from dictionary of events and dictionary of times, then checks
        if needed amount of data points is present. If not enough data points for given session ID
        deletes entries for that log.
        Last loop calculates times and appends to dict of events from dict of times needed data points
        @param dict_of_events dictionary that will be filled with events
        @param dict_of_times dictionary that will be filled with times
    """
    key_list = []
    for key in dict_of_events:
        if len(dict_of_events[key]) != 5:
            key_list.append(key)

    for key in key_list:
        dict_of_events.pop(key)

    key_list = []
    for key in dict_of_times:
        if len(dict_of_times[key]) != 5:
            key_list.append(key)

    for key in key_list:
        dict_of_times.pop(key)

    for key in dict_of_times:
        dict_of_times[key].sort()
        first_time = dict_of_times[key][0]
        last_time = dict_of_times[key][len(dict_of_times[key])-1]
        dict_of_events.setdefault(key, []).append(first_time)
        first_time = first_time.split("T")[1]
        last_time = last_time.split("T")[1]
        result = last_time + " - " + first_time
        dict_of_events.setdefault(key, []).append(result)


def build_json(dict_of_events):
    """!
        Builds JSON print from dict of events
        @param dict_of_events dictionary that is filled with data points for JSON file
    """
    for key in dict_of_events:
        data_dict = [{
            "time:": {
                "start": dict_of_events[key][5],
                "duration": dict_of_events[key][6]
            },
            "sessionid": key,
            "client": dict_of_events[key][0].split("=")[1],
            "messageid": dict_of_events[key][1].split("=")[1],
            "address": {
                "from": dict_of_events[key][2].split("=")[1],
                "to": dict_of_events[key][3].split("=")[1]
            },
            "status": dict_of_events[key][4].split("=")[1]
        }]
        json_dump = json.dumps(data_dict, indent=4)
        print(json_dump)


if __name__ == '__main__':
    """!
        Starting point of a JSON script
    """
    filename = check_args(sys.argv)
    dict_of_events = {}
    dict_of_times = {}
    gather_data(filename, dict_of_events, dict_of_times)
    clean_data(dict_of_events, dict_of_times)
    build_json(dict_of_events)




